package com.fuse.canteen.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

import com.fuse.canteen.dto.ResponseDto;

@ControllerAdvice
public class CustomExceptionHandlerAdvice {

	@ExceptionHandler({ InternalServerError.class })
	public ResponseDto<?> internalServiceException(InternalServerError e) {
		return new ResponseDto<>("Internal Error", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler({ DataNotFoundException.class })
	public ResponseDto<?> handleDogsServiceException(DataNotFoundException e) {
		return new ResponseDto<>("sdfsdf", HttpStatus.ACCEPTED);
	}
}
