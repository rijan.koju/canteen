package com.fuse.canteen.dto;

import org.springframework.http.HttpStatus;

public class ResponseDto<T> {

	private String message;

	private T data;

	private HttpStatus status;

	public ResponseDto() {

	}

	public ResponseDto(T data, String message, HttpStatus status) {
		super();
		this.message = message;
		this.data = data;
		this.status = status;
	}

	public ResponseDto(String message, HttpStatus status) {
		super();
		this.message = message;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public T getData() {
		return data;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
