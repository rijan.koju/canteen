package com.fuse.canteen.util;

public enum OrderStatus {

	PENDING, INPROCESS, READY
}
