package com.fuse.canteen.util;

public enum UserRole {
	ROLE_ADMIN, ROLE_EMPLOYEE
}
