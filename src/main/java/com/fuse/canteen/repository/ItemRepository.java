package com.fuse.canteen.repository;

import org.springframework.data.repository.CrudRepository;

import com.fuse.canteen.entity.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
