package com.fuse.canteen.repository;

import org.springframework.data.repository.CrudRepository;

import com.fuse.canteen.entity.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
