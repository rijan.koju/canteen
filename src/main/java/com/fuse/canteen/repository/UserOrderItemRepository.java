package com.fuse.canteen.repository;

import org.springframework.data.repository.CrudRepository;

import com.fuse.canteen.entity.UserOrderItem;

public interface UserOrderItemRepository extends CrudRepository<UserOrderItem, Long> {
}
