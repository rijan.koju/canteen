package com.fuse.canteen.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.fuse.canteen.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	Optional<User> findByUserName(String userName);
}
