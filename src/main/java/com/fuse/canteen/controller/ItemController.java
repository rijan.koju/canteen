package com.fuse.canteen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fuse.canteen.dto.ResponseDto;
import com.fuse.canteen.entity.Item;
import com.fuse.canteen.service.ItemService;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@PostMapping("/save")
	public ResponseDto<Item> saveItem(@RequestBody Item item) {
		return new ResponseDto<Item>(itemService.save(item), "Item saved", HttpStatus.CREATED);
	}

	@GetMapping("/list")
	public ResponseDto<List<Item>> list() {
		return new ResponseDto<List<Item>>(itemService.list(), "Item List", HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseDto<Item> findById(@PathVariable("id") long id) {
		return new ResponseDto<Item>(itemService.findById(id), "Item Found", HttpStatus.FOUND);
	}

	@PutMapping("/update")
	public ResponseDto<Item> updateItem(@RequestBody Item item) {
		return new ResponseDto<Item>(itemService.update(item), "Item Updated", HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseDto<?> deleteById(@PathVariable("id") long id) {
		itemService.delete(id);
		return new ResponseDto<>("Item Deleted", HttpStatus.OK);
	}

}
