package com.fuse.canteen.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fuse.canteen.entity.User;
import com.fuse.canteen.repository.UserRepository;

@Service
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Optional<User> userOpt = userRepository.findByUserName(userName);
		if(userOpt.isPresent())
			return new CustomUserDetail(userOpt.get());
		else
		throw new UsernameNotFoundException("User Does Not Exist");
	}

}
