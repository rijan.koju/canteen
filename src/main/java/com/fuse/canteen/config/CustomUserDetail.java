package com.fuse.canteen.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fuse.canteen.entity.User;

public class CustomUserDetail implements UserDetails {

	private static final long serialVersionUID = -5574133863941036630L;

	private String userName;

	private String password;

	private List<GrantedAuthority> authorities;

	public CustomUserDetail() {
	}

	public CustomUserDetail(User user) {
		this.userName = user.getUserName();
		this.password = user.getPassword();
		this.authorities = new ArrayList<>(Arrays.asList(new SimpleGrantedAuthority(user.getUserRole().toString())));
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
