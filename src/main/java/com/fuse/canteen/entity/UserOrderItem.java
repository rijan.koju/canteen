package com.fuse.canteen.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_order_item")
public class UserOrderItem {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	private Item item;

	public long getId() {
		return id;
	}

	public Order getOrder() {
		return order;
	}

	public User getUser() {
		return user;
	}

	public Item getItem() {
		return item;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}
