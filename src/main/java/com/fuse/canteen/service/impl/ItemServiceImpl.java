package com.fuse.canteen.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuse.canteen.entity.Item;
import com.fuse.canteen.exception.DataNotFoundException;
import com.fuse.canteen.repository.ItemRepository;
import com.fuse.canteen.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemRepository;

	@Override
	public Item save(Item item) {
		return itemRepository.save(item);
	}

	@Override
	public Item update(Item item) {
		Item entity = findById(item.getId());
		entity.setName(item.getName());
		entity.setPrice(item.getPrice());
		return itemRepository.save(entity);

	}

	@Override
	public Item findById(long id) {
		Optional<Item> itemOpt = itemRepository.findById(id);
		if (itemOpt.isPresent())
			return itemOpt.get();
		else
			throw new DataNotFoundException("Item Does Not Exist");
	}

	@Override
	public void delete(long id) {
		itemRepository.deleteById(id);

	}

	@Override
	public List<Item> getItemForToday() {
		return null;
	}

	@Override
	public List<Item> list() {
		return  (List<Item>) itemRepository.findAll();
	}

}
