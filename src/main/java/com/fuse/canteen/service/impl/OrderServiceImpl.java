package com.fuse.canteen.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuse.canteen.entity.Order;
import com.fuse.canteen.exception.DataNotFoundException;
import com.fuse.canteen.repository.OrderRepository;
import com.fuse.canteen.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public Order save(Order order) {
		return orderRepository.save(order);
	}

	@Override
	public Order update(Order order) {
		return orderRepository.findById(order.getId()).map(entity -> {
			entity.setOrderNo(order.getOrderNo());
			entity.setCompleteTime(order.getCompleteTime());
			entity.setOrderTime(order.getOrderTime());
			entity.setOrderStatus(order.getOrderStatus());
			return entity;
		}).orElseThrow(() -> new DataNotFoundException("Order Not Found"));

	}

	@Override
	public Order findById(long id) {
		Optional<Order> orderOpt = orderRepository.findById(id);
		if (orderOpt.isPresent())
			return orderOpt.get();
		else
			throw new DataNotFoundException("Order Does Not Exist");
	}

	@Override
	public void delete(long id) {
		orderRepository.deleteById(id);

	}

	@Override
	public List<Order> orderList() {
		return (List<Order>) orderRepository.findAll();
	}

}
