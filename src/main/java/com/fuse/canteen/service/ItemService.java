package com.fuse.canteen.service;

import java.util.List;

import com.fuse.canteen.entity.Item;

public interface ItemService {

	Item save(Item item);

	Item update(Item item);

	Item findById(long id);

	void delete(long id);
	
	List<Item> getItemForToday();
	
	List<Item> list();

}
