package com.fuse.canteen.service;

import java.util.List;

import com.fuse.canteen.entity.Order;

public interface OrderService {

	Order save(Order order);

	Order update(Order order);

	Order findById(long id);

	void delete(long id);
	
	List<Order> orderList();
	

}
